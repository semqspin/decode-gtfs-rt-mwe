const fs = require('fs');
const path = require('path');
const protobuf = require('protobufjs');

protobuf.parse.defaults.keepCase = true;
const root = protobuf.loadSync(path.join(__dirname, 'gtfs-realtime.proto'));
const FeedMessage = root.lookupType('transit_realtime.FeedMessage');

const filename = path.resolve(process.argv[2]);
const body = fs.readFileSync(filename);
const feed = FeedMessage.decode(body);

console.log(JSON.stringify(feed, null, 2));
