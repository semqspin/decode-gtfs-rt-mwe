## install

```bash
git clone https://semqspin@bitbucket.org/semqspin/decode-gtfs-rt-mwe.git
cd decode-gtfs-rt-mwe
npm install
```

## run

```bash
node index.js [rt-filename]
```

## examples

```bash
node index.js rt-samples/sncb-gtfs-rt-1.bin
node index.js rt-samples/tec-trip-updates-20190114-1054.bin
```
